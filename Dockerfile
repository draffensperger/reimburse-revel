# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Install godep dependency manager
RUN go get github.com/tools/godep

# Install migrations manager
RUN go get bitbucket.org/liamstask/goose/cmd/goose

# Add go source code
ADD . /go/src/github.com/draffensperger/reimburse

# Restore dependencies and build the app
WORKDIR /go/src/github.com/draffensperger/reimburse
RUN godep restore
RUN go install

ENV REIMBURSE_LISTEN_PORT 8001
EXPOSE 8001

ENTRYPOINT goose -env $REIMBURSE_PG_ENV up && reimburse
