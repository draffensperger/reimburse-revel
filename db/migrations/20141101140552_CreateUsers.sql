
-- +goose Up
CREATE TABLE users (
	id int PRIMARY KEY,
	email varchar(255) NOT NULL UNIQUE
);
-- SQL in section 'Up' is executed when this migration is applied


-- +goose Down
DROP TABLE IF EXISTS users;
-- SQL section 'Down' is executed when this migration is rolled back

