package main

import (
    "github.com/golang/oauth2"
    "net/http"
    "html/template"
     "log"
    "io/ioutil"
    "os"
)

var notAuthenticatedTemplate = template.Must(template.New("").Parse(`
<html><body>
You have currently not given permissions to access your data. Please authenticate this app with the Google OAuth provider.
<form action="/authorize" method="POST"><input type="submit" value="Ok, authorize this app with my id"/></form>
</body></html>
`));

var userInfoTemplate = template.Must(template.New("").Parse(`
<html><body>
This app is now authenticated to access your Google user info.  Your details are:<br />
{{.}}
</body></html>
`));

// variables used during oauth protocol flow of authentication
var (
    code = ""
    token = ""
)

var conf *oauth2.Config

//This is the URL that Google has defined so that an authenticated application may obtain the user's info in json format
const profileInfoURL = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json"

func main() {
	var err error
	conf, err = oauth2.NewConfig(&oauth2.Options{
		    ClientID:     os.Getenv("REIMBURSE_GOOGLE_OAUTH_CLIENT_ID"),
		    ClientSecret: os.Getenv("REIMBURSE_GOOGLE_OAUTH_CLIENT_SECRET"),
		    RedirectURL:  os.Getenv("REIMBURSE_BASE_URL") + "/oauth2callback",
		    Scopes:       []string{"https://www.googleapis.com/auth/userinfo.profile"},
		},
	    "https://accounts.google.com/o/oauth2/auth",
	    "https://accounts.google.com/o/oauth2/token")
	if err != nil {
	    log.Fatal(err)
	}
	
    http.HandleFunc("/", handleRoot)
    http.HandleFunc("/authorize", handleAuthorize)

    //Google will redirect to this page to return your code, so handle it appropriately
    http.HandleFunc("/oauth2callback", handleOAuth2Callback)
    
    host := os.Getenv("REIMBURSE_LISTEN_HOST")
    if host == "" {
        host = "localhost"
    }  
    port := os.Getenv("REIMBURSE_LISTEN_PORT")
    if port == "" {
        port = "8080"
    }
    host_and_port := host + ":" + port
    log.Printf("Reimburse Server listening on: " + host_and_port)

    log.Fatal(http.ListenAndServe(host_and_port, nil))
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
    notAuthenticatedTemplate.Execute(w, nil)
}

// Start the authorization process
func handleAuthorize(w http.ResponseWriter, r *http.Request) {
    //Get the Google URL which shows the Authentication page to the user
    //url := oauthCfg.AuthCodeURL("")
	url := conf.AuthCodeURL("state", "online", "auto")

    //redirect user to that page
    http.Redirect(w, r, url, http.StatusFound)
}

// Function that handles the callback from the Google server
func handleOAuth2Callback(w http.ResponseWriter, r *http.Request) {
    //Get the code from the response
    code := r.FormValue("code")    
    
    log.Printf("code: " + code)
    log.Printf("conf: %#v", conf)

    token, err := conf.Exchange(code)
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("got token %#v", token)

    transport := conf.NewTransport()
    transport.SetToken(token)
        
    log.Printf("got new transport %#v", transport)
	
    client := http.Client{Transport: transport}
    log.Printf("got client")
    
    //client.Get(profileInfoURL)
    
    var resp *http.Response
    resp, err= client.Get(profileInfoURL)
    log.Printf("got profile")
    if err != nil {
        log.Fatal(err)
    }
    
    defer resp.Body.Close()   
    
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatal(err)
    }
    log.Printf("read body")
    
    log.Printf("body: %#v", body)
        
    userInfoTemplate.Execute(w, string(body))
}
